module.exports = {
    Titles: {
      loginTitle: 'Увійти до спільноти «Критики»',
      startText: 'Вітаємо, '
    },

    users : {
      title: 'Krytyka',
      correctData: {
        email: 'fia.stiker@gmail.com',
        password: '123456789',
        name: 'Поліна Передовенко'
      },
      failData: {
        fail_email: 'fia.stiker+3@pegfund.com',
        password: 'Test123!!'
      },
    },

    browser : {
        url_address: "https://krytyka.com/ua",
        timeout : 3000000
    },

    screenshot: {
      path_ConnectBrowser: 'screenshots/connect_browser/',
      path_afterLogin: 'screenshots/after_login/'
    },

    error_text: {
      error_password: 'Password or email incorrect'
    }
}
