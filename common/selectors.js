module.exports = {
   logo: 'aria/КРИТИКА BETA',
   authorization : '.authorization__link',

   login : {
     email: 'input#email',
     password: 'input#password',
     loginTitle : '.login__title',
     btnLogin: '.btn-primary',
    },

    menu : {
      start: '.account__name',
      accountName: '.account__name a',
      submenu: '.account-submenu'
    },

    logout : {
      //btnLogout : 'aria/Вихід'
      btnLogout : '.account-submenu__item:nth-child(6) > a'
    }

}
