'use strict';
const puppeteer = require('puppeteer');
const testConst = require('../const.js');
const selectors = require('../../common/selectors');


const width = 1920
const height = 1080

let browser;
let page;


class Browser{

  static async set_up_Puppeteer(headless) {
    return Promise.resolve(
      browser = await puppeteer.launch({
        ignoreHTTPSErrors: true,
        headless: headless,
        slowMo: 250,
        timeout: 0,
        args: [
          '--no-sandbox',
          '--disable-dev-shm-usage',
          `--window-size=${width},${height}`
        ]
      }),
      expect(browser.isConnected()).toBe(true),
    ).catch(e=>{})
  }

  static async see_webPage(browser) {
    return Promise.resolve(

      page = await browser.newPage(),
      await page.setViewport({ width, height }),
      await page.goto(testConst.browser.url_address, {
        waitUntil: 'networkidle2',
        timeout: testConst.browser.timeout
      }),
      await page.title(testConst.users.title),
      expect(true).toBe(true),
      await page.waitForSelector(selectors.logo, testConst.browser.timeout),
      expect(true).toBe(true),
      await page.waitForSelector(selectors.authorization)

    ).catch(e=>{})
  }

}

module.exports=Browser;
