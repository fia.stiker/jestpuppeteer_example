'use strict';
//const puppeteer = require('puppeteer');
const testConst = require('./const.js');
const selectors = require('./selectors.js');


let bodyHandle;
let boundingBox;

class Actions{


  static async make_screenshot(page, folder, name) {
    return Promise.resolve(
      // full page screenshot
      bodyHandle = await page.$('body'),
      boundingBox = await bodyHandle.boundingBox(),

      await page.screenshot({
        path: folder + name,
        clip: {
          x: 0,
          y: 0,
          width: parseInt(boundingBox.width),
          height: parseInt(boundingBox.height)
        },
        type: 'jpeg'
      }),

      await bodyHandle.dispose()
    ).catch(e=>{})
  }

  //static async fillField(page, locator, text) {
    static async fillField(page, selector, text) {
    let element;
    return Promise.resolve(

      element = await page.waitForSelector(selector),
      await element.click(),
      await element.type(text)

      /*await page.waitForSelector(locator, testConst.browser.timeout),
      await page.focus(locator),
      await page.keyboard.type(text)*/
    ).catch(e=>{})
  }


}

module.exports=Actions;
