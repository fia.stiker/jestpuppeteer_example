'use strict'
//const puppeteer = require('puppeteer');
const testConst = require('../../common/const')
const selectors = require('../../common/selectors')
const action = require('../../common/actions')
const br = require('../../common/browser/browser')
const Auth = require('../../common/auth/auth')

let page
let browser

const email_correctData =  testConst.users.correctData.email
const password_correctData = testConst.users.correctData.password

  beforeAll(async () => {
    // set up Puppeteer
    try {

      browser = await br.set_up_Puppeteer(true)
      console.log('browser.isConnected')
      // Web page
      page = await br.see_webPage(browser)

      console.log('browser ready')
    } catch (e) {
      console.log(e)
      console.log('Error launching browser')
    }

  }, testConst.browser.timeout)

  afterAll(async () => {
    try {
      await browser.close()
    } catch (e) {
      console.log(e)
      console.log('Error close browser')
    }
  }, testConst.browser.timeout)

  describe('login on system', () => {

    test('go to login page', async () => {
        try {
          console.log("go to login page")
          await page.click(selectors.authorization).then(async ()=>{
            await page.waitForSelector(selectors.login.email)
            await page.waitForSelector(selectors.login.password)
            const loginTitle = await page.$eval(selectors.login.loginTitle, e => e.innerHTML)
            expect(loginTitle).toBe(testConst.Titles.loginTitle)
          })
        }catch (e) {
          console.log(e)
          console.log("error login page")
        }
      }, testConst.browser.timeout)


      test('login user ' + email_correctData, async () => {
        try {
          console.log('fill field email and click login btn')
          await Auth.login( page, email_correctData, password_correctData)
          // full page screenshot
          await action.make_screenshot(page, testConst.screenshot.path_afterLogin, 'after_login.jpg')
          console.log('logged')
        } catch (e) {
          console.log(e)
          console.log("error login")
        }
      }, testConst.browser.timeout)

      test('logout ', async () => {
        try {
          console.log('click logoutBtn')
          await Auth.logout( page)
        }catch (e) {
           console.log(e)
           console.log("error logout")
        }
      }, testConst.browser.timeout)

  })
