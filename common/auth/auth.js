'use strict';
//const puppeteer = require('puppeteer');
const testConst = require('../const.js');
const selectors = require('../../common/selectors');
const action = require('../../common/actions');

class Auth{

  static async login( page, emails_correctData, password) {

    return Promise.resolve(
       await action.fillField(page, selectors.login.email, emails_correctData),
       await action.fillField(page, selectors.login.password, password),
       await page.click(selectors.login.btnLogin).then(async()=>{
         await page.waitForSelector(selectors.menu.accountName)
         const startTitle = await page.$eval(selectors.menu.accountName, e => e.innerHTML)
         expect(startTitle).toBe(testConst.users.correctData.name + '!')

       })
    ).catch(e=>{})
  }

  static async logout(page) {
    return Promise.resolve(
       await page.click(selectors.menu.accountName),
       await page.waitForSelector(selectors.logout.btnLogout),
       await page.click(selectors.logout.btnLogout).then(async ()=>{
          await page.waitForSelector(selectors.authorization)
       })
    ).catch(e=>{})
  }

}

module.exports=Auth;
